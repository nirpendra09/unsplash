import React, { useState } from 'react'
import './Card.css'



const Card = ({ image, full, regular, small }) => {
    const [download, setDownload] = useState(false);

    const onClickHandler = () => {
        setDownload(!download)
        console.log(download)
    }


    return (
        <div className="card">
            <img src={ image } onClick={ onClickHandler } alt="" />

            { download &&
                <div>
                    <a href={ full } className='btn'> Full </a>
                    <a href={ regular } className='btn'> Regular </a>
                    <a href={ full } className='btn'> Small </a>

                </div>
            }
        </div>
    )
}

export default Card
